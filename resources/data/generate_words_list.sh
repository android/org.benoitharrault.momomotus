#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "$(dirname "${CURRENT_DIR}")")"

TARGET_SOURCE_CODE_FILE="${BASE_DIR}/lib/data/game_data.dart"

echo "class GameData {" >${TARGET_SOURCE_CODE_FILE}
echo "  static const Map<String, dynamic> data = {" >>${TARGET_SOURCE_CODE_FILE}
echo "    \"words\": {" >>${TARGET_SOURCE_CODE_FILE}

for LANG in fr; do
  echo "${LANG}"
  echo "      \"${LANG}\": {" >>${TARGET_SOURCE_CODE_FILE}
  for LENGTH in 4 5 6 7 8; do
    echo "${LANG} ${LENGTH}"
    echo "        \"${LENGTH}\": {" >>${TARGET_SOURCE_CODE_FILE}
    for TYPE in dictionary easy normal; do
      FILE="${CURRENT_DIR}/words/words-${LENGTH}-${LANG}-${TYPE}.txt"

      # Get errors
      PATTERN='^[A-Z]{'"${LENGTH}"'}$'
      ERRORS="$(cat "${FILE}" | grep -v -E "${PATTERN}")"

      # Force fix/clean file
      TMP="${FILE}.tmp"
      cat "${FILE}" | tr a-z A-Z | grep -E "${PATTERN}" | sort | uniq >"${TMP}"
      mv "${TMP}" "${FILE}"

      # Count
      COUNT="$(cat "${FILE}" | wc -l | awk '{print $1}')"
      echo "${LANG} ${LENGTH} ${TYPE} => ${COUNT}"

      echo "          \"${TYPE}\": [" >>${TARGET_SOURCE_CODE_FILE}
      for WORD in $(cat ${FILE} | sort); do
        echo "            \"${WORD}\"," >>${TARGET_SOURCE_CODE_FILE}
      done
      echo "          ]," >>${TARGET_SOURCE_CODE_FILE}
    done
    echo "        }," >>${TARGET_SOURCE_CODE_FILE}
  done
  echo "      }," >>${TARGET_SOURCE_CODE_FILE}
done

echo "    }," >>${TARGET_SOURCE_CODE_FILE}
echo "  };" >>${TARGET_SOURCE_CODE_FILE}
echo "}" >>${TARGET_SOURCE_CODE_FILE}

dart fix --apply ${TARGET_SOURCE_CODE_FILE}
