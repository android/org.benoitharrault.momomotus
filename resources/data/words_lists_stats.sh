#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
PROJECT_DIR="$(dirname "${CURRENT_DIR}")"

ASSETS_FOLDER="${PROJECT_DIR}/assets/files"

for LANG in fr; do
  echo "${LANG}"
  for LENGTH in 4 5 6 7 8; do
    echo "${LANG} ${LENGTH}"
    for TYPE in dictionary easy normal; do
      FILE="${ASSETS_FOLDER}/words-${LENGTH}-${LANG}-${TYPE}.txt"

      # Get errors
      PATTERN='^[A-Z]{'"${LENGTH}"'}$'
      ERRORS="$(cat "${FILE}" | grep -v -E "${PATTERN}")"

      # Force fix/clean file
      TMP="${FILE}.tmp"
      cat "${FILE}" | tr a-z A-Z | grep -E "${PATTERN}" | sort | uniq >"${TMP}"
      mv "${TMP}" "${FILE}"

      # Count
      COUNT="$(cat "${FILE}" | wc -l | awk '{print $1}')"
      echo "${LANG} ${LENGTH} ${TYPE} => ${COUNT}"
    done
  done
done
