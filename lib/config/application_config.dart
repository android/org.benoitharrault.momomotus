import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:momomotus/cubit/activity/activity_cubit.dart';

import 'package:momomotus/ui/pages/game.dart';

import 'package:momomotus/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: skin
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDefault = 'default';

  // activity parameter: lang
  static const String parameterCodeLang = 'activity.lang';
  static const String langValueFr = 'fr';

  // activity parameter: word length
  static const String parameterCodeWordLength = 'activity.wordLength';
  static const String wordLengthValue4 = '4';
  static const String wordLengthValue5 = '5';
  static const String wordLengthValue6 = '6';
  static const String wordLengthValue7 = '7';
  static const String wordLengthValue8 = '8';

  // activity parameter: difficulty level
  static const String parameterCodeDifficultyLevel = 'activity.difficultyLevel';
  static const String difficultyLevelValueEasy = 'easy';
  static const String difficultyLevelValueNormal = 'normal';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  // game configuration
  static const int maxGuessesCount = 7;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Momomotus',
    activitySettings: [
      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDefault,
            isDefault: true,
          ),
        ],
      ),

      // lang
      ApplicationSettingsParameter(
        code: parameterCodeLang,
        values: [
          ApplicationSettingsParameterItemValue(
            value: langValueFr,
            isDefault: true,
          ),
        ],
      ),

      // word length
      ApplicationSettingsParameter(
        code: parameterCodeWordLength,
        values: [
          ApplicationSettingsParameterItemValue(
            value: wordLengthValue4,
            color: Colors.green,
            text: '4️⃣',
          ),
          ApplicationSettingsParameterItemValue(
            value: wordLengthValue5,
            color: Colors.yellow,
            text: '5️⃣',
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: wordLengthValue6,
            color: Colors.orange,
            text: '6️⃣',
          ),
          ApplicationSettingsParameterItemValue(
            value: wordLengthValue7,
            color: Colors.red,
            text: '7️⃣',
          ),
          ApplicationSettingsParameterItemValue(
            value: wordLengthValue8,
            color: Colors.purple,
            text: '8️⃣',
          ),
        ],
      ),

      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueEasy,
            color: Colors.green,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueNormal,
            color: Colors.orange,
          ),
        ],
        customPainter: (context, value) => ParameterPainterDifficultyLevel(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
