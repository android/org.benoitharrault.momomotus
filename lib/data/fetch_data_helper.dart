import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:momomotus/data/game_data.dart';

class FetchDataHelper {
  FetchDataHelper();

  Map<String, Map<String, Map<String, List<String>>>> _words = {};
  Map<String, Map<String, Map<String, List<String>>>> get words => _words;

  void init() {
    try {
      // Map<LANG, Map<LENGTH, Map<TYPE, List<WORD>>>> data = {};
      Map<String, Map<String, Map<String, List<String>>>> data = {};

      final Map<String, dynamic> dataPerLang = GameData.data['words'] as Map<String, dynamic>;

      dataPerLang.forEach((lang, rawDataPerLength) {
        Map<String, Map<String, List<String>>> dataForThisLang = {};

        final Map<String, dynamic> dataPerLength = rawDataPerLength as Map<String, dynamic>;
        dataPerLength.forEach((length, rawDataPerType) {
          Map<String, List<String>> dataForTheseLangAndLength = {};

          final Map<String, dynamic> dataPerType = rawDataPerType as Map<String, dynamic>;
          dataPerType.keys.toList().forEach((String type) {
            List<String> wordsList = [];

            final List<dynamic> rawWordsList = dataPerType[type] as List<dynamic>;
            for (var item in rawWordsList) {
              wordsList.add(item.toString());
            }

            dataForTheseLangAndLength[type] = wordsList;
          });

          dataForThisLang[length] = dataForTheseLangAndLength;
        });
        data[lang] = dataForThisLang;
      });

      _words = data;
    } catch (e) {
      printlog("$e");
    }
  }

  List<String> getDictionary({
    required String lang,
    required String length,
  }) {
    if (_words.isEmpty) {
      init();
    }

    final List<String> dictionary = _words[lang]?[length]?['dictionary'] ?? [];
    if (dictionary.isEmpty) {
      printlog('Did not find any word for dictionary $lang/$length.');
    }

    return dictionary;
  }

  String getRandomWord({
    required String lang,
    required String length,
    required String level,
  }) {
    if (_words.isEmpty) {
      init();
    }

    final List<String> list = _words[lang]?[length]?[level] ?? [];
    if (list.isEmpty) {
      printlog('Did not find any word for list $lang/$length/$level.');
    }

    return list.elementAt(Random().nextInt(list.length - 1));
  }
}
