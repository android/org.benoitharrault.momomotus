import 'package:flutter/material.dart';

import 'package:momomotus/ui/widgets/game/key.dart';

class KeyboardWidget extends StatelessWidget {
  const KeyboardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final List<List<String>> keys = [
      ['A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
      ['Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M'],
      ['<', ' ', 'W', 'X', 'C', 'V', 'B', 'N', ' ', '!'],
    ];

    List<TableRow> tableRows = [];
    for (var row in keys) {
      List<TableCell> tableCells = [];
      for (var key in row) {
        tableCells.add(TableCell(
          child: KeyWidget(caption: key),
        ));
      }
      tableRows.add(TableRow(children: tableCells));
    }

    return SizedBox(
      height: 150,
      width: double.maxFinite,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 2),
        padding: const EdgeInsets.all(2),
        child: Table(
          defaultVerticalAlignment: TableCellVerticalAlignment.middle,
          children: tableRows,
        ),
      ),
    );
  }
}
