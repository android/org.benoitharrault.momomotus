import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:momomotus/cubit/activity/activity_cubit.dart';

class KeyWidget extends StatelessWidget {
  const KeyWidget({
    super.key,
    required this.caption,
  });

  final String caption;

  @override
  Widget build(BuildContext context) {
    String keyText = caption;
    if (caption == '<') {
      keyText = '⬅️';
    } else if (caption == '!') {
      keyText = '☑️';
    }

    const Color keyColor = Colors.black;

    if (caption == ' ') {
      return const SizedBox();
    }

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        const Image(
          image: AssetImage('assets/ui/key.png'),
          fit: BoxFit.fill,
        ),
        Center(
          child: TextButton(
            style: TextButton.styleFrom(
              padding: const EdgeInsets.all(0),
            ),
            child: Text(
              keyText,
              style: const TextStyle(
                color: keyColor,
                fontSize: 30.0,
                fontWeight: FontWeight.w800,
              ),
              textAlign: TextAlign.center,
            ),
            onPressed: () {
              if (caption == '<') {
                BlocProvider.of<ActivityCubit>(context).currentGuessRemoveLetter();
              } else if (caption == '!') {
                BlocProvider.of<ActivityCubit>(context).submitWord();
              } else if (caption != ' ') {
                BlocProvider.of<ActivityCubit>(context).currentGuessAddLetter(caption);
              }
            },
          ),
        ),
      ],
    );
  }
}
