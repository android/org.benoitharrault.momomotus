import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:momomotus/config/application_config.dart';

import 'package:momomotus/cubit/activity/activity_cubit.dart';
import 'package:momomotus/models/activity/activity.dart';

class GameCellWidget extends StatelessWidget {
  const GameCellWidget({
    super.key,
    required this.cellValue,
    required this.cellTip,
    required this.hasFocus,
    required this.foundLetter,
  });

  final String cellValue;
  final String cellTip;
  final bool hasFocus;
  final String foundLetter;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final String skin =
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeSkin);

        const Color textColor = Colors.white;
        final Color focusBorderColor = Colors.yellow.shade700;
        const Color defaultBorderColor = Colors.white;

        String cellImage = 'empty';
        if (cellTip != '') {
          cellImage = cellTip;
        }

        String displayedCellValue = cellValue;

        if ((foundLetter != ' ') && (cellValue == ' ')) {
          displayedCellValue = foundLetter;
          cellImage = 'good';
        }

        final Image imageWidget = Image(
          image: AssetImage('assets/skins/${skin}_$cellImage.png'),
          fit: BoxFit.fill,
        );

        final Widget cellBackground = Container(
          decoration: BoxDecoration(
            border: Border.all(
              width: 4.0,
              color: hasFocus ? focusBorderColor : defaultBorderColor,
              style: BorderStyle.solid,
            ),
          ),
          child: imageWidget,
        );

        Text textWidget = Text(
          displayedCellValue,
          style: const TextStyle(
            color: textColor,
            fontSize: 40.0,
            fontWeight: FontWeight.w900,
          ),
          textAlign: TextAlign.center,
        );

        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
            cellBackground,
            Center(child: textWidget),
          ],
        );
      },
    );
  }
}
