import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:momomotus/config/application_config.dart';
import 'package:momomotus/cubit/activity/activity_cubit.dart';
import 'package:momomotus/models/activity/activity.dart';
import 'package:momomotus/ui/widgets/game/game_cell.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;
        final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

        const int maxGuessesCount = ApplicationConfig.maxGuessesCount;
        final int wordLength = int.parse(
            currentActivity.activitySettings.get(ApplicationConfig.parameterCodeWordLength));

        final List<String> guesses = currentActivity.guesses;

        List<TableRow> tableRows = [];
        for (int lineIndex = 0; lineIndex < maxGuessesCount; lineIndex++) {
          String word = '';
          if (lineIndex < guesses.length) {
            word = guesses[lineIndex];
          } else if (lineIndex == guesses.length) {
            word = currentActivity.currentGuess;
          }

          final List<String> tips = activityCubit.getTips(word);

          List<TableCell> tableCells = [];
          for (int colIndex = 0; colIndex < wordLength; colIndex++) {
            String cellValue = ' ';
            if (word.length > colIndex) {
              cellValue = word[colIndex];
            }

            String cellTip = '';
            if (lineIndex < guesses.length) {
              cellTip = tips[colIndex];
            }

            final bool hasFocus = (!currentActivity.foundWord) &&
                (lineIndex == guesses.length) &&
                (colIndex == word.length);

            final String foundLetter =
                ((!currentActivity.foundWord) && (lineIndex == guesses.length))
                    ? currentActivity.foundLetters.substring(colIndex, colIndex + 1)
                    : ' ';

            tableCells.add(TableCell(
              child: GameCellWidget(
                cellValue: cellValue,
                cellTip: cellTip,
                hasFocus: hasFocus,
                foundLetter: foundLetter,
              ),
            ));
          }

          tableRows.add(TableRow(children: tableCells));
        }

        List<Widget> gameBoard = [
          Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: TableBorder.all(
              color: Colors.white,
              style: BorderStyle.none,
            ),
            children: tableRows,
          ),
        ];

        double horizontalMargins = 10;
        if (wordLength < 6) {
          horizontalMargins = 40;
          if (wordLength < 5) {
            horizontalMargins = 60;
          }
        }

        // Failed -> show word
        if (currentActivity.isFinished && !currentActivity.gameWon) {
          gameBoard.add(Text(
            currentActivity.word,
            style: const TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ));
        }

        return Container(
          margin: EdgeInsets.symmetric(horizontal: horizontalMargins),
          padding: const EdgeInsets.all(2),
          child: Column(
            children: gameBoard,
          ),
        );
      },
    );
  }
}
