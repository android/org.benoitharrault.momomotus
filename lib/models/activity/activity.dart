import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:momomotus/config/application_config.dart';
import 'package:momomotus/data/fetch_data_helper.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.animationInProgress = false,

    // Base data
    required this.word,
    required this.dictionary,

    // Game data
    this.currentGuess = '',
    this.foundLetters = '',
    required this.guesses,
    this.foundWord = false,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool animationInProgress;

  // Base data
  final String word;
  final List<String> dictionary;

  // Game data
  String currentGuess;
  String foundLetters;
  List<String> guesses;
  bool foundWord;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      word: '',
      dictionary: [],
      // Game data
      guesses: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final String pickedWord = FetchDataHelper().getRandomWord(
      lang: newActivitySettings.get(ApplicationConfig.parameterCodeLang),
      length: newActivitySettings.get(ApplicationConfig.parameterCodeWordLength),
      level: newActivitySettings.get(ApplicationConfig.parameterCodeDifficultyLevel),
    );

    final List<String> dictionary = FetchDataHelper().getDictionary(
      lang: newActivitySettings.get(ApplicationConfig.parameterCodeLang),
      length: newActivitySettings.get(ApplicationConfig.parameterCodeWordLength),
    );

    // Starts with first letter revealed
    final String foundLetters = pickedWord.substring(0, 1).padRight(pickedWord.length, ' ');

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      word: pickedWord,
      dictionary: dictionary,
      // Game data
      foundLetters: foundLetters,
      guesses: [],
    );
  }

  bool get canBeResumed => isStarted && !isFinished;
  bool get gameWon => isRunning && isStarted && isFinished;
  bool get isFinished {
    if (foundWord || (guesses.length >= ApplicationConfig.maxGuessesCount)) {
      return true;
    }

    return false;
  }

  bool checkWordIsValid(String candidate) {
    final int length =
        int.parse(activitySettings.get(ApplicationConfig.parameterCodeWordLength));

    return (word.length == length) &&
        (candidate.length == length) &&
        dictionary.contains(candidate);
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    word: $word');
    printlog('    dictionary: ${dictionary.length}');
    printlog('  Game data');
    printlog('    currentGuess: $currentGuess');
    printlog('    foundLetters: $foundLetters');
    printlog('    guesses: $guesses');
    printlog('    foundWord: $foundWord');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'word': word,
      'dictionary': dictionary,
      // Game data
      'currentGuess': currentGuess,
      'foundLetters': foundLetters,
      'guesses': guesses,
      'foundWord': foundWord,
    };
  }
}
